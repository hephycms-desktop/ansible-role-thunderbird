Role Name
=========

Install the mail client and enable TLS1.0 for CERN IMAP

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

    - hosts: desktop
      roles:
         - dietrichliko.thunderbird

License
-------

MIT

Author Information
------------------

Dietrich.Liko@oeaw.ac.at
